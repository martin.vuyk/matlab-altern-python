# Instalador Jupyter Notebooks + librerias open source para reemplazar a Matlab
Porque todos nos volvemos zurdos cuando nos piden abrir la billetera

#### Instalacion

- Descargar el zip del codigo del repositorio
- Descomprimir
- Ejecutar ./instalar (.bat windows, .sh linux o mac)


#### Dependencias

Tener python >= 3.7 instalado
    - instalar con este [link](https://www.python.org/downloads/)
